import React from "react";
import ReacDOM from "react-dom";
import Main from "./Main";
import "./index.css";

ReacDOM.render(
    <Main />,
    document.getElementById("root")
);